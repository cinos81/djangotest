#-*- coding: utf-8 -*-
from django.forms.models import ModelForm
from member.models import MemberModel
# from django.db.models.fields import IntegerField, EmailField, CharField
# from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
from django.db.models.fields import CharField
from django.forms.forms import Form
from django.contrib.auth.models import User

class MemberForm(ModelForm):
    username = CharField(max_length=100)
    password = CharField(max_length=100)

    #테스트를 위해서 name필드에 대한 벨리데이션 로직을 추가
    def clean_username(self):
        #정제된 로그인 정보를 취득
        username = self.cleaned_data['username']
        if len(username) < 3:
            raise ValidationError('유저명은 3글자를 넘어야 합니다.')
        return username

    #디버깅을 용이하게 하기 위해 __unicode__메소드를 구현
    def __unicode__(self):
        return '{username}'.format(username=self.username)

#ModelForm의 경우
    class Meta:
#         이렇게 연결하면 한번에 모델 생성이 되지만
#         model = User
# 이렇게 래퍼를 통해서 이 프로젝트에서 활성화된 유저 모델을 가져오는 것이 좋다고 한다. 
# 그 이유인즉 디폴트 유저모델을 바꿀 수 있기 때문임
        model = get_user_model()
# django 최신 버젼에서는 아래와 같이 적어줄 필요가 있다.
        fields = '__all__'