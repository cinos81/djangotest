from django.db import models
from django.db.models.fields import CharField, EmailField, IntegerField

# Create your models here.

class MemberModel(models.Model):
    name  = CharField(max_length=50)
    email = EmailField()
    age = IntegerField()
    
    def __unicode__(self):
        return '%s, %s, %s' % (self.name, self.email, self.age)
    
    class Meta:
        db_table = 'members'