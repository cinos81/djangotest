#-*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect
#-*- coding: utf-8 -*-
from member.models import MemberModel
from django.http.response import HttpResponse
from django.views.generic.base import View
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.template.context import RequestContext
from member.forms import MemberForm
from django.contrib.auth import authenticate, login

# Create your views here.

class Login(View):
    def get(self, req):
        context = {}
        context['authenticationForm'] = AuthenticationForm()
        return render_to_response('member/login.html', 
                          context,
                          context_instance=RequestContext(req))
    
    def post(self, req):
        username = req.POST['username']
        password = req.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(req, user)
            return HttpResponse('로그인 성공')
        else:
            return redirect('/member/login/')
        
class Join(View):
    def get(self, req):
        context = {'userCreationForm':UserCreationForm()}
        return render_to_response('member/join.html',context,
                              context_instance=RequestContext(req))
        
    def post(self, req):
        userCreationForm = UserCreationForm(req.POST)
        if userCreationForm.is_valid():
            userCreationForm.save()
            return HttpResponse('회원 가입 성공')
        else:
            return redirect('/member/join/')
