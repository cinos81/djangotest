from django.contrib import admin
from resume.models import ResumeModel


# Register your models here.

class ResumeAdmin(admin.ModelAdmin):
    class Meta:
        model = ResumeModel

admin.site.register(ResumeModel, ResumeAdmin)