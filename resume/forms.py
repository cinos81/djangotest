# -*- coding: utf-8 -*-
'''
Created on 2014. 5. 9.

@author: cinos81
'''
from resume.models import ResumeModel
from django.core.exceptions import ValidationError
from django.forms.models import ModelForm

class ResumeForm(ModelForm):

#clean_<필드명>
#특정 필드의 벨리데이션 작업을 처리한다. 
    def clean_name(self):
#cleaned_data메소드는 is_valid메소드가 호출된 이후에 생성된다.
        name =  self.cleaned_data['name']
        if len(name) < 3:
#사용자 정의 폼에서 에러를 확정하고 싶으면 에러를 발생시킬것 
            raise ValidationError('user name length must be bigger than 3chars')
        else:
            return name
    
    class Meta:
        model = ResumeModel
        fields = '__all__'