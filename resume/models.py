# -*- coding:utf-8 -*-
from django.db.models.base import Model
from django.db.models.fields import CharField
# 이 필드가 아니라 django.db 아래 네임스페이스의 필드를 사용해야 하는 것에 주의
# from django.forms.fields import CharField

# Create your models here.
class ResumeModel(Model):
    name  = CharField(max_length=50)
    
    #__unicode__메소드를 구현해서 디버깅 출력을 다듬는다.
    def __unicode__(self):
        return '%s' % self.name
    
    class Meta:
        db_table = 'resumes'