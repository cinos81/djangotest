# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.http.response import HttpResponse
from resume.forms import ResumeForm
from resume.models import ResumeModel
from django.template.context import RequestContext
from django.views.generic.base import View

# Create your views here.

class ResumeView(View):
    def get(self, req, userID):
        # $.extend와 같은 동작을 한다.
        # 본디 GET파라메터만으로 충분하지만 uri를 시그널로 사용하기 때문에 이런 구조가 되었음.
        getParam = dict(req.GET.items() + {'name':userID}.items())
        resumeForm = ResumeForm(getParam)
        if not resumeForm.is_valid():
            #입력값이 이상한 경우 새로운 폼을 리턴해서 재 입력을 유도한다.
            #자바스크립트의 개입을 제한한다는 점에서는 나쁘지 않은 선택이라고 생각함.
            resumeForm = ResumeForm()
        context = {'resumeForm':resumeForm, 'message':'보내기'}
        return render_to_response('resume/resume.html', 
                                   context, 
                                   context_instance=RequestContext(req, processors=[]))
    def post(self, req, userID):
        resumeForm = ResumeForm(req.POST)
        #입력값이 확실한 경우
        if resumeForm.is_valid():
            #Form.cleaned_data메소드롤 사용해서 정제된 데이터를 취득한다.
            cleanedName = resumeForm.cleaned_data['name']
            #기존에 존재하는 레코드롤 추출
            resume = ResumeModel.objects.filter(name=userID)
            if resume:
                #기존 레코드를 수정하는 경우
                resume.update(name=cleanedName)
                message = '값 수정 성공'
            else:
                #새로운 레코드를 삽입하는 경우
                resumeForm.save()
                message = '값 추가 성공'
            return HttpResponse(message)
        else:
            return HttpResponse('필수적인 폼 값이 없습니다.')
